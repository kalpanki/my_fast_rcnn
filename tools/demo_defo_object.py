#!/home/isit/anaconda2/bin/python

# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""
Demo script showing detections in sample images.

See README.md for installation instructions before running.
"""

import _init_paths
from fast_rcnn.config import cfg
from fast_rcnn.test import im_detect
from utils.cython_nms import nms
from utils.timer import Timer
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import caffe, os, sys, cv2
import argparse
import dlib
import xml.etree.ElementTree as ET

#CLASSES = ('__background__',
#          'object_1', 'object_2', 'object_3', 'object_4', 'object_5')
CLASSES = ('__background__', 'Target Object')

NETS = {'vgg16': ('VGG16',
                  'vgg16_fast_rcnn_iter_40000.caffemodel'),
        'vgg_cnn_m_1024': ('VGG_CNN_M_1024',
                           'vgg_cnn_m_1024_fast_rcnn_iter_40000.caffemodel'),
        'caffenet': ('CaffeNet',
                     'caffenet_fast_rcnn_iter_60000123213_final.caffemodel')}


def vis_detections(im, class_name, dets, img_idx, thresh=0.5):
    """Draw detected bounding boxes."""
    inds = np.where(dets[:, -1] >= thresh)[0]
    if len(inds) == 0:
        print('no target detected!')
        return

    im = im[:, :, (2, 1, 0)]
    fig, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(im, aspect='equal')
    for i in inds:
        bbox = dets[i, :4]
        score = dets[i, -1]
        print "Detections is: \n\n"
        print dets

        ax.add_patch(
            plt.Rectangle((bbox[0], bbox[1]),
                          bbox[2] - bbox[0],
                          bbox[3] - bbox[1], fill=False,
                          edgecolor='yellow', linewidth=8)
            )
        # top left corner
        # plt.plot(bbox[0], bbox[1], 'bs')
        # bottom right
        # plt.plot(bbox[2], bbox[3], 'bs')
        ax.text(bbox[0], bbox[1] - 2,
                '{:s} {:.3f}'.format(class_name, score),
                bbox=dict(facecolor='blue', alpha=0.5),
                fontsize=14, color='white')

    ax.set_title(('{} detections with '
                  'p({} | box) >= {:.1f}').format(class_name, class_name,
                                                  thresh),
                  fontsize=14)
    plt.axis('off')
    plt.tight_layout()
    plt.draw()
    fig_name = '/home/isit/Downloads/real/' + 'img_' + str(img_idx) + '.jpg'
    plt.savefig(fig_name)


def demo(net, object_name, classes):
    """Detect object classes in an image using pre-computed object proposals."""

    # number of images to demo
    num_demo_imgs = 1620 -770
    pr_bboxes = np.zeros((1,7))
    # Load pre-computed Selected Search object proposals
    # box_file = os.path.join(cfg.ROOT_DIR, 'data', 'demo_defo_object', 'test.mat')
    # obj_proposals = sio.loadmat(box_file)['all_boxes']

    # iterate files and do detection
    test_set_file = '/home/isit/workspace/DefoObjInWild/dataset/ImageSets/test.txt'
    with open(test_set_file) as f:
        lines = f.readlines()

    for i in range(num_demo_imgs):
        i = i + 770
        # Load the demo image
        #im_file = os.path.join(cfg.ROOT_DIR, 'data', 'demo_defo_object', image_name + '.jpg')
        # im_file = os.path.join('/home/isit/workspace/object_dataset/data/Images/',
        #                          object_name + '_img' + str(1001 + i) + '.jpg')
        curr_line = lines[i]
        curr_line = curr_line.strip('\n')
        im_file = os.path.join('/home/isit/workspace/DefoObjInWild/dataset/TestImages/' + curr_line + '.jpg')
        print ('Image file is: ', im_file)

        im = cv2.imread(im_file)

        # get the proposals for the image using dlib library 
        # and detect with those
        props = []
        rects = []
        dlib.find_candidate_object_locations(im, rects,min_size=500)
        for k,d in enumerate(rects):
            props.append([d.left(),d.top(),d.right(),d.bottom()])
        props = np.array(props)
        scores, boxes = im_detect(net, im, props)
        # scores, boxes = im_detect(net, im, obj_proposals[0,i])

        # Detect all object classes and regress object bounds
        # timer = Timer()
        #timer.tic()
        #scores, boxes = im_detect(net, im, obj_proposals)
        # scores, boxes = im_detect(net, im, obj_proposals[0,i])
        # timer.toc()
        # print ('Detection took {:.3f}s for '
        #        '{:d} object proposals').format(timer.total_time, boxes.shape[0])

        # Visualize detections for each class
        CONF_THRESH = 0.85
        NMS_THRESH = 0.3
        for cls in classes:
            cls_ind = CLASSES.index(cls)
            cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
            cls_scores = scores[:, cls_ind]
            keep = np.where(cls_scores >= CONF_THRESH)[0]
            cls_boxes = cls_boxes[keep, :]
            cls_scores = cls_scores[keep]
            dets = np.hstack((cls_boxes,cls_scores[:, np.newaxis])).astype(np.float32)
            keep = nms(dets, NMS_THRESH)
            dets = dets[keep, :]
            # print 'All {} detections with p({} | box) >= {:.1f}'.format(cls, cls,CONF_THRESH)

            # deal with detections properly
            temp_bboxes = []
            if dets.size > 0: 
                for ii in range(len(dets)):
                    a = np.hstack((np.squeeze(dets[ii,:]), 0, i)).astype(np.float32)
                    print ("Appending", a)
                    temp_bboxes.append(a)
                    #temp_bboxes.append([np.squeeze(dets[ii,:]), 0, i])
            else:
                # temp_bboxes.append(np.array([0,0,0,0,0,0]).astype(np.uint))
                temp_bboxes.append([0,0,0,0,0,0,0])
            vis_detections(im, cls, dets, i, thresh=CONF_THRESH)
            tl = len(temp_bboxes)
            l,x = np.shape(pr_bboxes)
            pr_bboxes = np.resize(pr_bboxes,[l+tl, 7])
            for jj in range(tl):
                pr_bboxes[l+jj,:] = temp_bboxes[jj]
        # finally save results
        pr_dest = '/home/isit/workspace/DefoObjInWild/pr_bboxes.mat'
        sio.savemat(pr_dest, {'pr_bboxes' : pr_bboxes})


def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Train a Fast R-CNN network')
    parser.add_argument('--gpu', dest='gpu_id', help='GPU device id to use [0]',
                        default=0, type=int)
    parser.add_argument('--cpu', dest='cpu_mode',
                        help='Use CPU mode (overrides --gpu)',
                        action='store_true')
    parser.add_argument('--net', dest='demo_net', help='Network to use [vgg16]',
                        choices=NETS.keys(), default='vgg16')

    args = parser.parse_args()

    return args


def get_gt_bboxes():
    test_set_file = '/home/isit/workspace/DefoObjInWild/dataset/ImageSets/test.txt'
    with open(test_set_file) as f:
        lines = f.readlines()
    # to store ground truth
    gt_bboxes = np.zeros((len(lines),5))

    for l in range(len(lines)):
        line = lines[l]
        line = line.strip('\n')
        print ("line is: ", line)
        xml_file = '/home/isit/workspace/DefoObjInWild/dataset/' + 'TestAnnotations/' + line + '.xml'
        tree = ET.parse(xml_file)
        root = tree.getroot()
        # get class index
        class_idx = (root[1][0].text).strip('object_')
        gt_bboxes[l,-1] = int(class_idx)
        # get the box locations
        for i in range(4):
            gt_bboxes[l,i] = int(root[1][1][i].text)
            print ("in for loop: ", gt_bboxes[l,i])

        return gt_bboxes




if __name__ == '__main__':
    args = parse_args()

    # prototxt = os.path.join(cfg.ROOT_DIR, 'models', NETS[args.demo_net][0],'test.prototxt')
    # prototxt = '/home/isit/workspace/fast-rcnn/models/DefoObject/test.prototxt'
    prototxt = '/home/isit/workspace/fast-rcnn/models/DefoObject/test.prototxt'

    print ("Prototxt file is: ", prototxt)
    # caffemodel = os.path.join(cfg.ROOT_DIR, 'output', 'default', 'train', NETS[args.demo_net][1])
    # caffemodel = '/home/isit/workspace/fast-rcnn/output/default/defo_obj_fast_rcnn_iter_40000.caffemodel'
    caffemodel = '/home/isit/workspace/fast-rcnn/output/default/train/defo_obj_fast_rcnn_iter_100000.caffemodel'
    # caffemodel = '/home/isit/workspace/DefoObjInWild/results_final/incr_conditions/growing_training_set/trained_model/defo_obj_fast_rcnn_iter_1000_14.caffemodel'
 
    if not os.path.isfile(caffemodel):
        raise IOError(('{:s} not found.\nDid you run ./data/scripts/'
                       'fetch_fast_rcnn_models.sh?').format(caffemodel))

    if args.cpu_mode:
        caffe.set_mode_cpu()
    else:
        caffe.set_mode_gpu()
        caffe.set_device(args.gpu_id)

    net = caffe.Net(prototxt, caffemodel, caffe.TEST)

    print '\n\nLoaded network {:s}'.format(caffemodel)

    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print 'Starting demo for TEST images:'
    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    #demo(net, 'object1', ('object_1','object_2','object_3','object_4','object_5'))
    demo(net, 'object1', ('Target Object',))

    #print 'Demo for data/demo_defo_object/object2_img1150.jpg'
    #demo(net, 'object2_img1150', ('object_2'))

    # plt.show()
