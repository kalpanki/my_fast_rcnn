#!/home/isit/anaconda2/bin/python

# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""
Demo script showing detections in sample images.

See README.md for installation instructions before running.
"""

import _init_paths
from fast_rcnn.test import im_detect
from utils.cython_nms import nms
from utils.timer import Timer
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import caffe, os, sys, cv2
import argparse
import numpy.matlib
import time
import dlib


CLASSES = ('__background__','object_0')


def vis_detections(im, class_name, dets, thresh=0.5):
	"""Draw detected bounding boxes."""
	inds = np.where(dets[:, -1] >= thresh)[0]
	if len(inds) == 0:
		print('no target detected!')
		return

	im = im[:, :, (2, 1, 0)]
	fig, ax = plt.subplots(figsize=(12, 12))
	ax.imshow(im, aspect='equal')
	for i in inds:
		bbox = dets[i, :4]
		score = dets[i, -1]

		ax.add_patch(
			plt.Rectangle((bbox[0], bbox[1]),
						  bbox[2] - bbox[0],
						  bbox[3] - bbox[1], fill=False,
						  edgecolor='red', linewidth=3.5)
			)
		ax.text(bbox[0], bbox[1] - 2,
				'{:s} {:.3f}'.format(class_name, score),
				bbox=dict(facecolor='blue', alpha=0.5),
				fontsize=14, color='white')

	ax.set_title(('{} detections with '
				  'p({} | box) >= {:.1f}').format(class_name, class_name,
												  thresh),
				  fontsize=14)
	plt.axis('off')
	plt.tight_layout()
	plt.draw()																	


def parse_args():
	"""Parse input arguments."""
	parser = argparse.ArgumentParser(description='Train a Fast R-CNN network')
	parser.add_argument('--gpu', dest='gpu_id', help='GPU device id to use [0]',
						default=0, type=int)
	parser.add_argument('--cpu', dest='cpu_mode',
						help='Use CPU mode (overrides --gpu)',
						action='store_true')
	parser.add_argument('--path', dest='test_path',
						help = 'The path for test images')
	parser.add_argument('--prop', dest='proposals_file',
						help = 'The mat file with all proposals - test_proposals.mat')
	parser.add_argument('--prototxt', dest = 'prototxt_file',
						help = 'The prototxt file to use - test.prototxt')
	parser.add_argument('--model', dest = 'caffe_model',
						help = 'The caffe model file to use - *.caffemodel')
	parser.add_argument('--num_objects', dest = 'num_objects',
						help = 'The number of objects or test classes')
	parser.add_argument('--imgs_per_class', dest = 'imgs_per_class',
						help = 'Number of test imgaes in each class')
	parser.add_argument('--test_iter', dest = 'test_iter',
						help = 'Number of testing iteration')
	parser.add_argument('--cond', dest = 'cond',
						help = 'Testing condition')
	parser.add_argument('--difficulty', dest = 'difficulty',
						help = 'Testing difficulty')
	args = parser.parse_args()

	return args


def detect(net, classes, test_path, proposals_file, num_objects, imgs_per_class, test_iter, cond, difficulty):
	# path to the proposal file for test images
	#box_file = os.path.join(cfg.ROOT_DIR, 'data', 'demo_defo_object', 'test.mat')
	print("Proposal file is: ", proposals_file)
	obj_proposals = sio.loadmat(proposals_file)['all_boxes']

	num_objects = int(num_objects)
	# imgs_per_class = int(imgs_per_class)
	imgs_per_class = int(imgs_per_class)


	CONF_THRESH = 0.85
	# conf_iter = 11
	# for CONF_THRESH in ([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.95]):
	# for CONF_THRESH in ([0.85,0.88,0.9,0.91,0.92,0.93,0.94,0.95,0.96,0.97,0.98,0.99]):
	NMS_THRESH = 0.3
	del_order = ['no', 'easy', 'medium', 'hard']
	pr_bboxes = np.zeros((1,7))

	# iterate files and do detection
	test_set_file = test_path + '/test.txt'
	with open(test_set_file) as f:
		lines = f.readlines()

	# iterate each line in file
	# for img_idx in range(len(lines)):
	for img_idx in range(len(lines)):
		curr_line = lines[img_idx]
		curr_line = curr_line.strip('\n')
		# read image files by name
		im_file = os.path.join(test_path + '/' + '../TestImages/' + curr_line + '.jpg')
		print ('Testing image: ', im_file)
		im = cv2.imread(im_file)

		# get the proposals for the image using dlib library 
		# props = []
		# rects = []
		# dlib.find_candidate_object_locations(im, rects, min_size=500)
		# for k,d in enumerate(rects):
		# 	props.append([d.left(),d.top(),d.right(),d.bottom()])
		# props = np.array(props)
		# #do the actual detection
		# scores, boxes = im_detect(net, im, props)

		# do the detection using the selective_search matlab function 
		scores, boxes = im_detect(net, im, obj_proposals[0,img_idx])
		# pause to avoid computer crashing!
		# time.sleep(5)

		# now iterate each class and 
		found_flg = 0

		for cls in classes:
			cls_ind = CLASSES.index(cls)
			cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
			cls_scores = scores[:, cls_ind]
			keep = np.where(cls_scores >= CONF_THRESH)[0]
			cls_boxes = cls_boxes[keep, :]
			cls_scores = cls_scores[keep]
			# dets is bbox
			dets = np.hstack((cls_boxes,
							  cls_scores[:, np.newaxis])).astype(np.float32)
			keep = nms(dets, NMS_THRESH)
			dets = dets[keep, :]
			# if more than 1, we will just keep the first
			# if len(keep) > 1:
			# 	keep = [keep[0]]
			
			# dets = dets[keep, :]
			# print ('DETECTIONS ARE: ', dets)

			# add for precision=recall computation
			temp_bboxes = []
			if dets.size > 0: 
				for ii in range(len(dets)):
					a = np.hstack((np.squeeze(dets[ii,:]), 0, img_idx)).astype(np.float32)
					print ("Appending", a)
					temp_bboxes.append(a)
			else:
				# temp_bboxes.append(np.array([0,0,0,0,0,0]).astype(np.uint))
				# The object is present. But not retrieved 
				# False Negative
				temp_bboxes.append([0,0,0,0,0,1,img_idx])
			tl = len(temp_bboxes)
			l,x = np.shape(pr_bboxes)
			pr_bboxes = np.resize(pr_bboxes,[l+tl, 7])
			for jj in range(tl):
				pr_bboxes[l+jj,:] = temp_bboxes[jj]

				# populate the predicted boxes array
				# cls_ind - 1 because while detecting we have background as first class
				# temp_bboxes = np.hstack((np.squeeze(dets[:,:4]), cls_ind-1)).astype(np.uint)
				# temp_bboxes = np.hstack((np.squeeze(dets[:,:4]), 0)).astype(np.uint)
				# print temp_bboxes
				# found_flg = 1
				# print ("temp boxes are: " , temp_bboxes)
				# vis_detections(im, cls, dets, thresh=CONF_THRESH)
				# break
		# if not found a single detection, populate some dummy val
		# if not found_flg:
		#   	temp_bboxes = np.array([0,0,0,0,0]).astype(np.uint)
		
	# finally save the result
	# pr_dest = test_path + '/../pr_bboxes_' + str(conf_iter) + '.mat'
	pr_dest = test_path + '/../pr_bboxes' + '_' + cond + '_' + difficulty + '_' + str(test_iter) + '.mat'
	sio.savemat(pr_dest, {'pr_bboxes' : pr_bboxes})
	# conf_iter += 1



if __name__ == '__main__':

	# Parse all the arguments to the file
	args = parse_args()
	# if args.cpu_mode:
	# 	caffe.set_mode_cpu()
	# else:
	caffe.set_mode_gpu()
	caffe.set_device(args.gpu_id)
	
	# create the network with files provided
	net = caffe.Net(args.prototxt_file, args.caffe_model, caffe.TEST)
	print '\n\nLoaded network successfully \n\n'

	# do the detection and save results as *.mat file
	detect(net, ('object_0',), \
		   args.test_path, \
		   args.proposals_file, \
		   args.num_objects, \
		   args.imgs_per_class, \
		   args.test_iter, \
		   args.cond, \
		   args.difficulty)

	# plt.show()