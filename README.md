# README #

This is a modified version of the Fast R-CNN repository. The original algorithm was published in [this](http://arxiv.org/abs/1504.08083) paper. 

The original Fast R-CNN repository can be found [here](https://github.com/rbgirshick/fast-rcnn).

### What is this repository for? ###

* This repository is for training Fast R-CNN on our own "Deformable Objects" dataset. 

### How do I get set up? ###

* Checkout the repositry.
* Compile the caffe library.
* Compile the cython library.
* Finally run the demo to see if the installation works.

### Who do I talk to? ###

* Email [me](vasan.shrini@gmail.com)